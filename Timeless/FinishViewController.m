//
//  FinishViewController.m
//  Timeless
//
//  Created by Glenn Thrope on 1/26/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//
// TODO: Combine this class with PlayVideoViewController?

#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>

#import "FinishViewController.h"

@interface FinishViewController()
@property (strong, nonatomic) MPMoviePlayerController *player;
@end

@implementation FinishViewController

@synthesize MovieUrl = _MovieUrl;
@synthesize player = _player;

- (IBAction)copyToIPad:(UIButton *)sender {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:self.MovieUrl]) {
        [library writeVideoAtPathToSavedPhotosAlbum:self.MovieUrl completionBlock:^(NSURL *assetURL, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert;
                if (error) {
                    alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                } else {
                    alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                }
                [alert show];
            });
        }];
    }
}

- (IBAction)comingSoon {
    [[[UIAlertView alloc] initWithTitle:@"Coming soon!" message:@"This feature has not yet been implemented." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // without this audio doesn't work. what a pain in the ass.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    self.player = [[MPMoviePlayerController alloc] initWithContentURL:self.MovieUrl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerLoadStateChanged:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    
    [self.player prepareToPlay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)moviePlayerLoadStateChanged:(NSNotification*)notification
{
    // remove notification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    
    if ([self.player loadState] != MPMovieLoadStateUnknown) {
        self.player.controlStyle = MPMovieControlStyleEmbedded;
        
        CGSize videoSize = CGSizeMake(500, 375);
        CGSize frameSize = self.view.frame.size;
        
        [self.player.view setFrame:CGRectMake(frameSize.width/2 - videoSize.width/2, frameSize.height/2 - videoSize.height/2 - 25, videoSize.width, videoSize.height)];
        [self.view addSubview:self.player.view];
        
        [self.player play];
    }
}

@end
