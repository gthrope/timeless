//
//  UIImagePickerController+Nonrotating.m
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "UIImagePickerController+Nonrotating.h"

@implementation UIImagePickerController (Nonrotating)

- (BOOL) shouldAutorotate {
    return NO;
}

@end
