//
//  QuestionsViewController.h
//  Timeless
//
//  Created by Glenn Thrope on 1/27/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionsViewController : UIViewController
@property (nonatomic, strong) NSMutableArray *MoviePaths;
@end
