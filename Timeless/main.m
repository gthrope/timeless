//
//  main.m
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TimelessAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TimelessAppDelegate class]));
    }
}
