//
//  FinishViewController.h
//  Timeless
//
//  Created by Glenn Thrope on 1/26/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface FinishViewController : UIViewController
@property (nonatomic, strong) NSURL *MovieUrl;
@end
