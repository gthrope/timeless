//
//  TimelessAppDelegate.h
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelessAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
