//
//  QuestionsViewController.m
//  Timeless
//
//  Created by Glenn Thrope on 1/27/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import <MobileCoreServices/UTCoreTypes.h>

#import "QuestionsViewController.h"

@interface QuestionsViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic) NSInteger currentQuestion;
@end

@implementation QuestionsViewController

@synthesize MoviePaths = _MoviePaths;
@synthesize currentQuestion = _currentQuestion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recordVideo:(UIButton *)sender {
    
    // cache current question number
    self.currentQuestion = sender.tag;
    
    // Get image picker
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = self;
    cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    // 3 - Display image picker
    [self presentViewController:cameraUI animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        
        // remember moviePath
        [self.MoviePaths replaceObjectAtIndex:self.currentQuestion withObject:moviePath];
    }
}


@end
