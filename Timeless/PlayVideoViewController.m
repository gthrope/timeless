//
//  PlayVideoViewController.m
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//
// TODO: Combine this class with FinishViewController?

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#import "PlayVideoViewController.h"

@interface PlayVideoViewController()
@property (strong, nonatomic) MPMoviePlayerController *player;
@end

@implementation PlayVideoViewController

@synthesize player = _player;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // without this audio doesn't work. what a pain in the ass.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"trailer" ofType:@"mov" inDirectory:@"Movies"]];
    self.player = [[MPMoviePlayerController alloc] initWithContentURL:url];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerLoadStateChanged:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    
    [self.player prepareToPlay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)moviePlayerLoadStateChanged:(NSNotification*)notification
{
    // remove notification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    
    if ([self.player loadState] != MPMovieLoadStateUnknown) {
        self.player.controlStyle = MPMovieControlStyleEmbedded;
        
        CGSize videoSize = CGSizeMake(500, 375);
        CGSize frameSize = self.view.frame.size;
        
        [self.player.view setFrame:CGRectMake(frameSize.width/2 - videoSize.width/2, frameSize.height/2 - videoSize.height/2 + 50, videoSize.width, videoSize.height)];

/*
        CGSize videoSize = CGSizeMake(400, 300);
        CGSize frameSize = self.view.frame.size;
        
        [self.player.view setFrame:CGRectMake(frameSize.width/2 - videoSize.width/2, frameSize.height/2 - videoSize.height/2 + 50, videoSize.width, videoSize.height)];
  */
        [self.view addSubview:self.player.view];
 
        [self.player play];
    }
}

@end
