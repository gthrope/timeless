//
//  TimelessViewController.m
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "TimelessViewController.h"

@implementation TimelessViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

@end
