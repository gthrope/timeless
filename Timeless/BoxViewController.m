//
//  QuestionsViewController.m
//  Timeless
//
//  Created by Glenn Thrope on 1/24/13.
//  Copyright (c) 2013 Glenn Thrope. All rights reserved.
//

#import "BoxViewController.h"
#import "QuestionsViewController.h"
#import "FinishViewController.h"

#define NUM_CATEGORIES 6
#define NUM_QUESTIONS_PER_CATEGORY 3

@interface BoxViewController ()
@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic) NSInteger currentCategory;
@property NSURL *movieUrl;
@property (weak, nonatomic) IBOutlet UIProgressView *finalizeProgressView;
@property (strong, nonatomic) AVAssetExportSession *exporter;
@property (strong, nonatomic) NSTimer *finalizeTimer;
@end

@implementation BoxViewController

@synthesize categories = _categories;
@synthesize currentCategory = _currentCategory;
@synthesize movieUrl = _movieUrl;
@synthesize finalizeProgressView = _finalizeProgressView;
@synthesize exporter = _exporter;

- (NSMutableArray *)categories
{
    if(!_categories) {
        _categories = [[NSMutableArray alloc] initWithCapacity:NUM_CATEGORIES];
        for(int categoryIndex=0; categoryIndex<NUM_CATEGORIES; categoryIndex++) {
            NSMutableArray *category = [[NSMutableArray alloc] initWithCapacity:NUM_QUESTIONS_PER_CATEGORY];
            [_categories addObject:category];
            for(int questionIndex=0; questionIndex<NUM_QUESTIONS_PER_CATEGORY; questionIndex++) {
                [category addObject:[NSNull null]];
            }
        }
    }
    return _categories;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectCategory:(UIButton *)sender {
    self.currentCategory = sender.tag;
    [self performSegueWithIdentifier:@"Questions" sender:self];
}

- (IBAction)comingSoon {
    [[[UIAlertView alloc] initWithTitle:@"Coming soon!" message:@"This feature has not yet been implemented." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}


- (IBAction)finishMovie {

    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        
    CMTime cumulativeDuration = kCMTimeZero;
        
    NSURL *titleVideoUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Title" ofType:@"m4v" inDirectory:@"Movies"]];
    AVAsset *titleMovieAsset = [AVAsset assetWithURL:titleVideoUrl];
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, titleMovieAsset.duration) ofTrack:[[titleMovieAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:cumulativeDuration error:nil];
        
    cumulativeDuration = CMTimeAdd(cumulativeDuration, titleMovieAsset.duration);
    
    for(int categoryIndex=0; categoryIndex<self.categories.count; categoryIndex++) {
        
        NSArray *category = self.categories[categoryIndex];
        
        for(int questionIndex=0; questionIndex<category.count; questionIndex++)
        {
            if([category objectAtIndex:questionIndex] == [NSNull null]) {
                continue;
            }
        
            // add interstitial video
            NSString *movieName = [NSString stringWithFormat:@"Q%d", questionIndex];
            NSURL *interstitialVideoUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:movieName ofType:@"m4v" inDirectory:@"Movies"]];
            AVAsset *interstitialMovieAsset = [AVAsset assetWithURL:interstitialVideoUrl];
            [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, interstitialMovieAsset.duration) ofTrack:[[interstitialMovieAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:cumulativeDuration error:nil];
            cumulativeDuration = CMTimeAdd(cumulativeDuration, interstitialMovieAsset.duration);
            
            AVAsset *movieAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:[category objectAtIndex:questionIndex]]];
            // add video
            [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, movieAsset.duration) ofTrack:[[movieAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:cumulativeDuration error:nil];
            // add audio
            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, movieAsset.duration) ofTrack:[[movieAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:cumulativeDuration error:nil];
            
            cumulativeDuration = CMTimeAdd(cumulativeDuration, movieAsset.duration);
        }
    }
    
//    NSArray *paths = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSUserDomainMask];
//    NSString *tempDirectory = [paths objectAtIndex:0];
    
    NSString *moviePath =  [NSTemporaryDirectory() stringByAppendingPathComponent: [NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
    self.movieUrl = [NSURL fileURLWithPath:moviePath];

    // set up progress bar
    self.finalizeTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(updateExportDisplay) userInfo:nil repeats:YES];
    self.finalizeProgressView.hidden = NO;
    
    // Create exporter
    self.exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    self.exporter.outputURL=self.movieUrl;
    self.exporter.outputFileType = AVFileTypeQuickTimeMovie;
    self.exporter.shouldOptimizeForNetworkUse = YES;
    [self.exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinish:self.exporter];
        });
    }];
}

-(void)exportDidFinish:(AVAssetExportSession*)session {
    if (session.status == AVAssetExportSessionStatusCompleted) {

        // clean up progress bar
        [self.finalizeTimer invalidate];
        self.finalizeProgressView.hidden = YES;
        
        [self performSegueWithIdentifier:@"Finish" sender:self];
    }
}

-(void) updateExportDisplay {
    self.finalizeProgressView.progress = self.exporter.progress;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Finish"]) {
        [segue.destinationViewController setMovieUrl:self.movieUrl];
    }
    if([segue.identifier isEqualToString:@"Questions"]) {
        [segue.destinationViewController setMoviePaths:[self.categories objectAtIndex:self.currentCategory]];
    }
}

@end
